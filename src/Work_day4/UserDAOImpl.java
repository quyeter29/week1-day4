package Work_day4;

import java.util.ArrayList;
import java.util.Scanner;

public class UserDAOImpl implements UserDAO {

	TaskDAO TaskDao = new TaskDAOImpl();
	public Scanner sc = new Scanner(System.in);

	@Override
	public ArrayList<Task> getAllTask() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Task GetTaskbyUser(String user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void AddTask() {
		String tt;
		do {
			TaskDao.addTask();
			System.out.println("Do you want to continous? Y or N");
			tt = sc.next();
		} while (tt.equals("Y"));

		if (tt.equals("N")) {
			Client client = new Client();
			client.menu();
		}
	}

	@Override
	public void UpdateTask() {
		String tt;
		do {
			TaskDao.updateTask();
			System.out.println("Do you want to continous? Y or N");
			tt = sc.next();
		} while (tt.equals("Y"));

		if (tt.equals("N")) {
			Client client = new Client();
			client.menu();
		}
	}

	@Override
	public void DeleteTask() {
		String tt;
		do {
			TaskDao.deleteTask();
			System.out.println("Do you want to continous? Y or N");
			tt = sc.next();
		} while (tt.equals("Y"));

		if (tt.equals("N")) {
			Client client = new Client();
			client.menu();
		}
	}

	@Override
	public void DisplaybyAssignTask(String assign) {
		if (TaskDao.getTaskbyAssign(assign).size() != 0) {
			System.out.println("Your task assigned");
			for (Task task : TaskDao.getTaskbyAssign(assign)) {
				System.out.println("Task " + task.getTaskId() + " - title: " + task.getTaskTitle() + " - text: "
						+ task.getTaskText() + " - assigned to: " + task.getAssignedTo());
			}
		} else {
			System.out.println("I can't find your task. Please check your username again!");
		}
	}

	@Override
	public void DisplayAllTask() {
		// print all Task
		for (Task task : TaskDao.getAllTask()) {
			System.out.println("Task " + task.getTaskId() + " - title: " + task.getTaskTitle() + " - text: "
					+ task.getTaskText() + " - assigned to: " + task.getAssignedTo());
		}

	}

	@Override
	public void DisplayResulftSearch() {
		String tt;
		do {
			System.out.println("Enter the task you want to search: ");
			String search = sc.nextLine();
			TaskDao.ResulftSearchTask(search);
			System.out.println("Do you want to continous? Y or N");
			tt = sc.next();
		} while (tt.equals("Y"));

		if (tt.equals("N")) {
			Client client = new Client();
			client.menu();
		}
	}

}

